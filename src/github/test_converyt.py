import tensorflow as tf
import os
from transformers import TFGPT2LMHeadModel, GPT2Config, GPT2Tokenizer
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2

# 加载GPT-2 tokenizer
tokenizer = GPT2Tokenizer.from_pretrained('gpt2')

# 加载GPT-2模型
model_dir = "C:/01.project/ipython/gpt-2/models/124M"
config = GPT2Config.from_json_file(os.path.join(model_dir, 'config.json'))
tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
model = TFGPT2LMHeadModel.from_pretrained(model_dir)

# 转换成TensorFlow 2.x的SavedModel格式
model_func = model.signatures['serving_default']
frozen_func = convert_variables_to_constants_v2(model_func)
graph_def = frozen_func.graph.as_graph_def(add_shapes=True)

# 创建SavedModel
export_dir = "C:/01.project/ipython/gpt-2/models/124M/saved_model"
builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
builder.add_graph(graph_def, [tf.saved_model.tag_constants.SERVING])
builder.save()
