import json
import os
import numpy as np
import tensorflow as tf
from transformers import GPT2Tokenizer, TFGPT2LMHeadModel


def interact_model(model_name="124",
                   models_dir='C:/01.project/ipython/gpt-2/models',
                   seed=None,
                   nsamples=1,
                   batch_size=1,
                   length=None,
                   temperature=1,
                   top_k=0):
    models_dir = os.path.expanduser(os.path.expandvars(models_dir))
    if batch_size is None:
        batch_size = 1
    assert nsamples % batch_size == 0
    tokenizer = GPT2Tokenizer.from_pretrained(model_name)
    model = TFGPT2LMHeadModel.from_pretrained(model_name)

    if length is None:
        length = model.config.n_positions // 2
    elif length > model.config.n_positions:
        raise ValueError("Can't get samples longer than window size: %s" % model.config.n_positions)

    while True:
        raw_text = input("Model prompt >>> ")
        while not raw_text:
            print('Prompt should not be empty!')
            raw_text = input("Model prompt >>> ")
        context_tokens = tokenizer.encode(raw_text, return_tensors='tf')

        np.random.seed(seed)
        tf.random.set_seed(seed)

        generated = 0
        for _ in range(nsamples // batch_size):
            out = model.generate(
                context_tokens,
                max_length=length + len(context_tokens[0]),
                temperature=temperature,
                top_k=top_k,
                num_return_sequences=batch_size
            )

            for i in range(batch_size):
                generated += 1
                text = tokenizer.decode(out[i][len(context_tokens[0]):], skip_special_tokens=True)
                print("=" * 40 + " SAMPLE " + str(generated) + " " + "=" * 40)
                print(text)
                print("=" * 80)
