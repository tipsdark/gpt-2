import tensorflow as tf
import numpy as np

# 加载GPT-2 124M模型
model_dir = "C:/01.project/ipython/gpt-2/models/124"
model = tf.saved_model.load(model_dir)

# 获取输入输出Tensor名称
input_name = model.signatures['serving_default'].inputs[0].name
output_name = model.signatures['serving_default'].outputs[0].name

# 设置生成文本的参数
batch_size = 1
length = 100
temperature = 1.0

# 生成文本的函数
def generate_text(input_text):
    encoded_text = np.array([encoder.encode(input_text)] * batch_size)
    generated = 0
    for _ in range(length // batch_size):
        outputs = model(input_name=encoded_text, temperature=temperature)
        preds = outputs[output_name][0, :, :].numpy()
        preds = np.argmax(preds, axis=-1)
        preds = np.squeeze(preds)
        generated += 1
        if generated == length:
            break
        encoded_text = np.concatenate((encoded_text[:, 1:], np.expand_dims(preds, axis=-1)), axis=-1)
    return encoder.decode(preds.tolist())
