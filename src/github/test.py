import tensorflow as tf

# 列出所有可用的物理设备
physical_devices = tf.config.list_physical_devices()

# 遍历物理设备，查找是否有GPU设备
for device in physical_devices:
    if 'GPU' in device.device_type:
        # 查看GPU是否支持CUDA
        print(tf.test.is_built_with_cuda())
        break
