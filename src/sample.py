"""
sample.py是GPT-2模型的推断代码，主要用于生成新的文本序列。具体来说，sample.py主要实现了以下功能：

1. 从输入的文本序列中随机选择一个子序列作为模型的起始输入；
2. 使用GPT-2模型的前向传播函数生成一个新的单词，并将该单词添加到原序列的末尾；
3. 重复上述步骤，生成指定长度的新文本序列。

具体实现细节如下：

1. 首先，sample.py中定义了一个Sampler类，该类接受一个GPT-2模型作为参数，用于生成新的文本序列；
2. Sampler类包含了一个生成函数（generate），该函数接受一个起始文本序列和要生成的文本长度作为输入，返回生成的新文本序列；
3. 生成函数的实现过程如下：
   - 从起始文本序列中随机选择一个子序列作为模型的起始输入；
   - 使用GPT-2模型的前向传播函数生成下一个单词，并将该单词添加到原序列的末尾；
   - 重复上述步骤，直到生成指定长度的新文本序列；
   - 返回新文本序列。
4. 在实际使用时，可以通过调用Sampler类的构造函数来创建一个Sampler对象，并使用该对象的generate函数生成新的文本序列。

总之，sample.py是GPT-2模型的推断代码，实现了基于GPT-2模型的文本生成功能，是GPT-2模型能够进行自然语言生成的重要组件。
"""
import tensorflow as tf

import model

def top_k_logits(logits, k):   ## 计算top_k
    if k == 0:
        # no truncation
        return logits

    def _top_k():
        values, _ = tf.nn.top_k(logits, k=k)
        min_values = values[:, -1, tf.newaxis]
        return tf.where(
            logits < min_values,
            tf.ones_like(logits, dtype=logits.dtype) * -1e10,
            logits,
        )
    return tf.cond(
       tf.equal(k, 0),
       lambda: logits,
       lambda: _top_k(),
    )


def top_p_logits(logits, p):       ### 计算top_p, 等于1时相当于没计算
    """Nucleus sampling"""
    batch, _ = logits.shape.as_list()
    sorted_logits = tf.sort(logits, direction='DESCENDING', axis=-1)
    cumulative_probs = tf.cumsum(tf.nn.softmax(sorted_logits, axis=-1), axis=-1)
    indices = tf.stack([
        tf.range(0, batch),
        # number of indices to include
        tf.maximum(tf.reduce_sum(tf.cast(cumulative_probs <= p, tf.int32), axis=-1) - 1, 0),
    ], axis=-1)
    min_values = tf.gather_nd(sorted_logits, indices)
    return tf.where(     # 若condition=True,则返回对应X的值，False则返回对应的Y值。
        logits < min_values,
        tf.ones_like(logits) * -1e10,
        logits,
    )


def sample_sequence(*, hparams, length, start_token=None, batch_size=None, context=None, temperature=1, top_k=0, top_p=1):
    if start_token is None:
        assert context is not None, 'Specify exactly one of start_token and context!'
    else:
        assert context is None, 'Specify exactly one of start_token and context!'
        context = tf.fill([batch_size, 1], start_token)      # 填充

    def step(hparams, tokens, past=None):
        lm_output = model.model(hparams=hparams, X=tokens, past=past, reuse=tf.AUTO_REUSE)    # tf.AUTO_REUSE共享变量作用域 节省内存空间

        logits = lm_output['logits'][:, :, :hparams.n_vocab]
        presents = lm_output['present']
        presents.set_shape(model.past_shape(hparams=hparams, batch_size=batch_size))
        return {
            'logits': logits,
            'presents': presents,
        }

    with tf.name_scope('sample_sequence'):
        def body(past, prev, output):
            next_outputs = step(hparams, prev, past=past)
            logits = next_outputs['logits'][:, -1, :]  / tf.to_float(temperature)      ## 只要最后一个输出的值（可能值的概率向量）
            logits = top_k_logits(logits, k=top_k)
            logits = top_p_logits(logits, p=top_p)
            samples = tf.multinomial(logits, num_samples=1, output_dtype=tf.int32)   ### 这里限制了仅仅采样一个值
            return [
                next_outputs['presents'] if past is None else tf.concat([past, next_outputs['presents']], axis=-2),
                samples,
                tf.concat([output, samples], axis=1)
            ]

        past, prev, output = body(None, context, context)

        def cond(*args):
            return True

        _, _, tokens = tf.while_loop(      # 循环  loop_vars既是输出值也是下次循环的输入值
            cond=cond, body=body,
            maximum_iterations=length - 1,
            loop_vars=[
                past,
                prev,
                output
            ],
            shape_invariants=[
                tf.TensorShape(model.past_shape(hparams=hparams, batch_size=batch_size)),
                tf.TensorShape([batch_size, None]),
                tf.TensorShape([batch_size, None]),
            ],
            back_prop=False,
        )

        return tokens
